# EnvSwitch

[![Latest Version on Packagist][ico-version]][link-packagist]
[![Total Downloads][ico-downloads]][link-downloads]
[![Build Status][ico-travis]][link-travis]
[![StyleCI][ico-styleci]][link-styleci]

This package provides an "artisan command" that can be used to switch between environments by changing the .env file.. Take a look at [contributing.md](contributing.md) to see a to do list.

This package is based on [Tommy Ku's][link-original-repository] repository. All credits for it.

## Installation

Via Composer

``` bash
$ composer require prefeituradeavare/envswitch
```

## Configuration

Add the line below on your `.gitignore`:
```
*.env
```

## Usage

### Saving an environment
```bash
$ php artisan env:switch --save
```

The command above will return something like: 
```bash
Environmental config file .local.env saved
```

This script uses `APP_ENV` value present on `.env` to name the new "env file"

### Switching environments
```bash
php artisan env:switch production
```

The command above will return something like: 
```bash
Successfully switched from local to production.
```
## Change log

Please see the [changelog](changelog.md) for more information on what has changed recently.

## Contributing

Please see [contributing.md](contributing.md) for details and a todolist.

## Security

If you discover any security related issues, please email dev@avare.sp.gov.br instead of using the issue tracker.

## Credits

- [Rodrigo de Souza][link-author]
- [All Contributors][link-contributors]  
  
## License

This project is released under the [MIT License][link-mit-license]. Please see the [license file](license.md) for more information.

[ico-version]: https://img.shields.io/packagist/v/prefeituradeavare/envswitch.svg?style=flat-square
[ico-downloads]: https://img.shields.io/packagist/dt/prefeituradeavare/envswitch.svg?style=flat-square
[ico-travis]: https://img.shields.io/travis/prefeituradeavare/envswitch/master.svg?style=flat-square
[ico-styleci]: https://gitlab.styleci.io/repos/11771125/shield

[link-packagist]: https://packagist.org/packages/prefeituradeavare/envswitch
[link-downloads]: https://packagist.org/packages/prefeituradeavare/envswitch
[link-travis]: https://travis-ci.org/prefeituradeavare/envswitch
[link-styleci]: https://gitlab.styleci.io/repos/11771125
[link-author]: https://gitlab.com/prefeituradeavare
[link-original-repository]: https://github.com/tommyku/laravel5_env
[link-contributors]: ../../contributors
[link-mit-license]: https://opensource.org/licenses/MIT