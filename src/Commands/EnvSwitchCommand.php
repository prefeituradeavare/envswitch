<?php

namespace PrefeituradeAvare\EnvSwitch\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

/**
 * Command based on project below:
 * https://github.com/tommyku/laravel5_env.
 *
 **** Usage
 ** Save the current .env into .$APP_ENV.env.
 *
 * $ php artisan env
 * Current application environment: local
 * $ php artisan env:switch --save
 * Environmental config file .local.env saved
 *
 ** Switch to another environment, given .$TARGET_ENV.env exists.
 *
 * $ php artisan env
 * Current application environment: test
 * $ php artisan env:switch local
 * Successfully switched from test to local.
 * $ php artisan env
 * Current application environment: local
 */
class EnvSwitchCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'env:switch';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Switch between environments by changing .env file';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $envPath = base_path().'/.env';
        $envir = $this->laravel['env'];
        if ($this->option('save')) {
            // save current env
            $targetPath = base_path().'/.'.$envir.'.env';
            File::put($targetPath, File::get($envPath), true);
            $this->info('Environmental config file <comment>.'.$envir.'.env</comment> saved');
        } else {
            // switch to a different env
            $targetEnv = $this->argument('env');
            $targetPath = base_path().'/.'.$targetEnv.'.env';
            if (! File::exists($targetPath)) {
                $this->error('Cannot switch to environment:<info> '.$targetEnv.' </info>because<info> .'.$targetEnv.'.env </info>doesn\'t exist');

                return;
            }
            File::put($envPath, File::get($targetPath), true);
            $this->info('Successfully switched from <comment>'.$envir.'</comment> to <comment>'.$targetEnv.'</comment>.');
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['env', InputArgument::OPTIONAL, 'The environment name to switch to'],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['save', null, InputOption::VALUE_NONE, 'Save the current .env to a \$APP_ENV.env file before switching.', null],
        ];
    }
}
