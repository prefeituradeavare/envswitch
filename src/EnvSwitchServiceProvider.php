<?php

namespace PrefeituradeAvare\EnvSwitch;

use Illuminate\Support\ServiceProvider;

class EnvSwitchServiceProvider extends ServiceProvider
{
    /**
     * The console commands.
     *
     * @var array
     */
    private $commands = [
        '\PrefeituradeAvare\EnvSwitch\Commands\EnvSwitchCommand',
    ];

    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        // $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'prefeituradeavare');
        // $this->loadViewsFrom(__DIR__.'/../resources/views', 'prefeituradeavare');
        // $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        // $this->loadRoutesFrom(__DIR__.'/routes.php');

        // Publishing is only necessary when using the CLI.
        if ($this->app->runningInConsole()) {
            $this->bootForConsole();
        }
    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__.'/../config/envswitch.php', 'envswitch');

        // Register the service the package provides.
        $this->app->singleton('envswitch', function ($app) {
            return new EnvSwitch;
        });

        $this->commands($this->commands);
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['envswitch'];
    }

    /**
     * Console-specific booting.
     *
     * @return void
     */
    protected function bootForConsole()
    {
        // Publishing the configuration file.
        $this->publishes([
            __DIR__.'/../config/envswitch.php' => config_path('envswitch.php'),
        ], 'envswitch.config');

        // Publishing the views.
        /*$this->publishes([
            __DIR__.'/../resources/views' => base_path('resources/views/vendor/prefeituradeavare'),
        ], 'envswitch.views');*/

        // Publishing assets.
        /*$this->publishes([
            __DIR__.'/../resources/assets' => public_path('vendor/prefeituradeavare'),
        ], 'envswitch.views');*/

        // Publishing the translation files.
        /*$this->publishes([
            __DIR__.'/../resources/lang' => resource_path('lang/vendor/prefeituradeavare'),
        ], 'envswitch.views');*/

        // Registering package commands.
        // $this->commands([]);
    }
}
