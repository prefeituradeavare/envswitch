# Changelog

All notable changes to `EnvSwitch` will be documented in this file.

## [1.1.0] - 2019-04-11
### Added
- License information
### Changed
- The package now registers the `env:switch` command automatically, so it is no longer necessary to edit the `App\Console\Kernel.php`

## [1.0.0] - 2019-04-10
### Added
- Everything